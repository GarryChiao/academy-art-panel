import Api from '@/services/Api'

export default {
  indexUserInfo (credentials) {
    return Api().post('indexUserInfo', credentials)
  },
  createUserInfo (credentials) {
    return Api().post('createUserInfo', credentials)
  },
  updateUserInfo (credentials) {
    return Api().post('updateUserInfo', credentials)
  },
  updateUserInfoLicenseImage (credentials) {
    return Api().post('updateUserInfoLicenseImage', credentials)
  },
  withdrawTotalEarnings (credentials) {
    return Api().post('withdrawTotalEarnings', credentials)
  },
  getWithdrawTotalEarnings (credentials) {
    return Api().post('getWithdrawTotalEarnings', credentials)
  },
  getDailyUserEarnings (credentials) {
    return Api().post('getDailyUserEarnings', credentials)
  },
  updateUserPhotoSticker (credentials) {
    return Api().post('updateUserPhotoSticker', credentials)
  },
  getMonthlyUserData (credentials) {
    return Api().post('getMonthlyUserData', credentials)
  }
}
