import Api from '@/services/Api'

export default {
  getAllUsers (credentials) {
    return Api().post('getAllUsers', credentials)
  },
  updateUser (credentials) {
    return Api().post('updateUser', credentials)
  },
  createUser (credentials) {
    return Api().post('createUser', credentials)
  }
}
