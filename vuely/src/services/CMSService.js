import Api from '@/services/Api'

export default {
    getContent (credentials) {
        return Api().post('getContent', credentials)
    },
    updateContent (credentials) {
        return Api().post('updateContent', credentials)
    },
    updateContentImage (credentials) {
        return Api().post('updateContentImage', credentials)
    },
    getClientImage (credentials) {
        return Api().post('getClientImage', credentials)
    },
    addClientImage (credentials) {
        return Api().post('addClientImage', credentials)
    },
    deleteClientImage (credentials) {
        return Api().post('deleteClientImage', credentials)
    }
}
