import Api from '@/services/Api'

export default {
  createEntryOrder (credentials) {
    return Api().post('createEntryOrder', credentials)
  },
  updateEntryOrderImageLink (credentials) {
    return Api().post('updateEntryOrderImageLink', credentials)
  },
  getEntryOrders (credentials) {
    return Api().post('getEntryOrders', credentials)
  },
  getAllEntryOrders (credentials) {
    return Api().post('getAllEntryOrders', credentials)
  },
  deleteEntryOrder (credentials) {
    return Api().post('deleteEntryOrder', credentials)
  }
}
