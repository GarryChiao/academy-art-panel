import Api from '@/services/Api'

export default {
  createInvestmentPlan (credentials) {
    return Api().post('createInvestmentPlan', credentials)
  },
  updatePendingInvestmentPlanImageLink (credentials) {
    return Api().post('updatePendingInvestmentPlanImageLink', credentials)
  },
  getInvestmentPlans (credentials) {
    return Api().post('getInvestmentPlans', credentials)
  },
  getConfirmedInvestmentPlans (credentials) {
    return Api().post('getConfirmedInvestmentPlans', credentials)
  },
  investmentPlanExpired (credentials) {
    return Api().post('investmentPlanExpired', credentials)
  }
}
