import Api from '@/services/Api'

export default {
  getMemberCount (credentials) {
    return Api().post('getMemberCount', credentials)
  },
  getTotalTSFDeposit (credentials) {
    return Api().post('getTotalTSFDeposit', credentials)
  }
}
