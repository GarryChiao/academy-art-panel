import Api from '@/services/Api'

export default {
  getAllUsers (credentials) {
    return Api().post('getAllUsers', credentials)
  },
  adminUpdateUser (credentials) {
    return Api().post('adminUpdateUser', credentials)
  },
  adminAddNewUser (credentials) {
    return Api().post('adminAddNewUser', credentials)
  },
  adminDeleteUser (credentials) {
    return Api().post('adminDeleteUser', credentials)
  },
  getLicensePendingUsers (credentials) {
    return Api().post('getLicensePendingUsers', credentials)
  },
  confirmLicensePendingUser (credentials) {
    return Api().post('confirmLicensePendingUser', credentials)
  }
}
