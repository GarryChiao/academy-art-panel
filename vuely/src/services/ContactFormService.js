import Api from '@/services/Api'

export default {
  getContactForm (credentials) {
    return Api().post('getContactForm', credentials)
  },
  deleteContactInfo (credentials) {
    return Api().post('deleteContactInfo', credentials)
  }
}
