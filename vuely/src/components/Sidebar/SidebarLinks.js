// Sidebar Routers
export const category1 = [
  {
    action: 'ti-dashboard',
    title: 'dashboard',
    items: [
      { title: 'dashboardv1', path: '/dashboard/dashboard-v1' },
      { title: 'dashboardv2', path: '/dashboard/dashboard-v2' }
    ]
  },
  {
    action: 'ti-dashboard',
    title: 'userProfile',
    path: '/users/user-profile'
  }
  // {
  //   action: 'ti-layers',
  //   title: 'pages',
  //   items: [
  //     { title: 'gallery', path: '/pages/gallery' },
  //     { title: 'pricing', path: '/pages/pricing' },
  //     { title: 'blank', path: '/pages/blank' }
  //   ]
  // },
  // {
  //   action: 'ti-timer',
  //   title: 'session',
  //   items: [
  //     { title: 'signUp', path: '/session/sign-up' },
  //     { title: 'login', path: '/session/login' },
  //     { title: 'lockScreen', path: '/session/lock-screen' }
  //   ]
  // }
]

export const category2 = [
  {
    action: 'ti-layout',
    title: 'myLessons',
    items: [
      { title: 'monthlyPaidLessons', path: '/lessons/monthly' },
      { title: 'singleLessons', path: '/lessons/single' },
      { title: 'cashLessons', path: '/lessons/cash' }
    ]
  }
]

export const category3 = [
  {
    action: 'ti-user',
    title: 'admin',
    items: [
      { title: 'manageUsers', path: '/admin/manage-users' }
    ]
  }
]

export const category4 = [
  {
    action: 'ti-pencil-alt',
    title: 'editor',
    items: [
      { title: 'quillEditor', path: '/editor/quilleditor' },
      { title: 'wYSIWYG', path: '/editor/wysiwyg' }
    ]
  },
  {
    action: 'ti-mouse-alt',
    title: 'dragAndDrop',
    items: [
      { title: 'vue2Dragula', path: '/drag-drop/vue2dragula' },
      { title: 'vueDraggable', path: '/drag-drop/vuedraggable' },
      { title: 'draggableResizeable', path: '/drag-drop/vuedraggableresizeable' }
    ]
  }
]
