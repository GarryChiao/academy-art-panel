const Joi = require('joi')
// Joi is to verify inputs
module.exports = {
  register (req, res, next){
    const schema = Joi.object().keys({
      email: Joi.string().email().required(),
      password: Joi.string().regex(
        new RegExp('^[a-zA-Z0-9]{8,32}$')
      ),
      phoneNumber: Joi.number(),// this place is not perfect,the phone number field must be a non-empty E.164 standard compliant identifier string
      displayName: Joi.string(),
      recommenderSerialNumber: Joi.any().optional()
    })

    const {error, value} = Joi.validate(req.body, schema)

    if(error){
      switch (error.details[0].context.key) {
        case 'email':
          res.status(400).send({
            error: 'You must provide a valid email address'
          })
          break
        case 'password':
          res.status(400).send({
            error: `The password provided failed to match the following rules:
            <br>
            1. It must contain ONLY the following characters: lower case, upper case or 0-9.
            <br>
            2. It must be at least 8 characters in length and not greater then 32 characters.
            `
          })
          break
        default:
          res.status(400).send({
            error: 'Invalid registration information.'
          })
      }
    }else {
      next()
    }
  }
}
