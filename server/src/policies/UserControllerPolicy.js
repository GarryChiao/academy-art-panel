const Joi = require('joi')
// Joi is to verify inputs
module.exports = {
  updateUserInfo (req, res, next){
    const schema = Joi.object().keys({
      formType: Joi.string(),
      realName: Joi.string(),
      gender: Joi.string(),
      birthDate: Joi.date(),
      licenseNumber: Joi.any(),
      licenseType: Joi.any(),
      address: Joi.any(),
      phoneNumber: Joi.any(),
      bankCode: Joi.any(),
      bankName: Joi.any(),
      bankAccount: Joi.any(),
      // 20180625 added field
      introducerCode: Joi.any(),
      uid: Joi.any()
    })

    const {error, value} = Joi.validate(req.body, schema)

    if(error){
      console.log(error)
      switch (error.details[0].context.key) {
        case 'realName':
          res.status(400).send({
            error: 'Your real name should be a string'
          })
          break
        case 'birthDate':
          res.status(400).send({
            error: 'Birth Date Error'
          })
          break
        default:
          res.status(400).send({
            error: 'Invalid User information.'
          })
      }
    }else {
      next()
    }
  }
}
