// // Authentication Part
// const AuthenticationController = require('./controllers/AuthenticationController')
// const AuthenticationControllerPolicy = require('./policies/AuthenticationControllerPolicy')
// // User Part
// const UserController = require('./controllers/UserController')
// const UserControllerPolicy = require('./policies/UserControllerPolicy')
// // Entry Order Part
// const EntryOrderController = require('./controllers/EntryOrderController')
// // Investment Plan Part
// const InvestmentPlanController = require('./controllers/InvestmentPlanController')
// // Administrator Part
// const Administrator = require('./controllers/AdministratorController')
// const ManageUser = require('./controllers/ManageUserController')
// const CMS = require('./controllers/CMSController')
// const Shareholder = require('./controllers/ShareholderController')
// const ContactForm = require('./controllers/ContactFormController')

const Admin = require('./controllers/AdminController')

module.exports = (app) => {
  // Admin
  app.post('/getAllUsers',
    Admin.getAllUsers
  )
  app.post('/updateUser',
    Admin.updateUser
  )
  app.post('/createUser',
    Admin.createUser
  )
  // // Authentication
  // app.post('/register',
  //   AuthenticationControllerPolicy.register,
  //   AuthenticationController.register
  // )
  // app.post('/login',
  //   AuthenticationController.login
  // )
  // // User
  // app.post('/indexUserInfo',
  //   UserController.indexUserInfo
  // )
  // // use the same one to create and update
  // app.post('/updateUserInfo',
  //   UserControllerPolicy.updateUserInfo,
  //   UserController.updateUserInfo
  // )
  // app.post('/updateUserInfoLicenseImage',
  //   UserController.updateUserInfoLicenseImage
  // )
  // app.post('/withdrawTotalEarnings',
  //   UserController.withdrawTotalEarnings
  // )
  // app.post('/getWithdrawTotalEarnings',
  //   UserController.getWithdrawTotalEarnings
  // )
  // app.post('/getDailyUserEarnings',
  //   UserController.getDailyUserEarnings
  // )
  // app.post('/updateUserPhotoSticker',
  //   UserController.updateUserPhotoSticker
  // )
  // app.post('/getMonthlyUserData',
  //   UserController.getMonthlyUserData
  // )
  // // Administrator
  // app.post('/admin',
  //   Administrator.index
  // )
  // app.post('/getAllPendingInvestmentPlans',
  //   Administrator.getAllPendingInvestmentPlans
  // )
  // app.post('/confirmPendingInvestmentPlan',
  //   Administrator.confirmPendingInvestmentPlan
  // )
  // app.post('/declinePendingInvestmentPlan',
  //   Administrator.declinePendingInvestmentPlan
  // )
  // app.post('/getEnvSettings',
  //   Administrator.getEnvSettings
  // )
  // app.post('/updateEnvSettings',
  //   Administrator.updateEnvSettings
  // )
  // app.post('/getAllPendingWithdrawTotalEarnings',
  //   Administrator.getAllPendingWithdrawTotalEarnings
  // )
  // app.post('/confirmPendingWithdrawTotalEarnings',
  //   Administrator.confirmPendingWithdrawTotalEarnings
  // )
  // app.post('/declinePendingWithdrawTotalEarnings',
  //   Administrator.declinePendingWithdrawTotalEarnings
  // )
  // app.post('/fetchSelectedUserInvestmentPlans',
  //   Administrator.fetchSelectedUserInvestmentPlans
  // )
  // app.post('/fetchSelectedUserEntryOrders',
  //   Administrator.fetchSelectedUserEntryOrders
  // )
  // app.post('/updateInvestmentPlanReturnRate',
  //   Administrator.updateInvestmentPlanReturnRate
  // )
  // app.post('/verifyInvestmentPlanWithoutRemittance',
  //   Administrator.verifyInvestmentPlanWithoutRemittance
  // )
  // // CMS
  // app.post('/getContent',
  //   CMS.getContent
  // )
  // app.post('/updateContent',
  //   CMS.updateContent
  // )
  // app.post('/updateContentImage',
  //   CMS.updateContentImage
  // )
  // app.post('/getClientImage',
  //   CMS.getClientImage
  // )
  // app.post('/addClientImage',
  //   CMS.addClientImage
  // )
  // app.post('/deleteClientImage',
  //   CMS.deleteClientImage
  // )
  // // contact form
  // app.post('/getContactForm',
  //   ContactForm.getContactForm
  // )
  // app.post('/deleteContactInfo',
  //   ContactForm.deleteContactInfo
  // )
  // // shareholder
  // app.post('/getMemberCount',
  //   Shareholder.getMemberCount
  // )
  // app.post('/getTotalTSFDeposit',
  //   Shareholder.getTotalTSFDeposit
  // )
  // // Manage Users
  // app.post('/getAllUsers',
  //   ManageUser.getAllUsers
  // )
  // app.post('/adminUpdateUser',
  //   ManageUser.adminUpdateUser
  // )
  // app.post('/adminAddNewUser',
  //   ManageUser.adminAddNewUser
  // )
  // app.post('/adminDeleteUser',
  //   ManageUser.adminDeleteUser
  // )
  // app.post('/getLicensePendingUsers',
  //   ManageUser.getLicensePendingUsers
  // )
  // app.post('/confirmLicensePendingUser',
  //   ManageUser.confirmLicensePendingUser
  // )
  // // entry orders
  // app.post('/createEntryOrder',
  //   EntryOrderController.createEntryOrder
  // )
  // app.post('/updateEntryOrderImageLink',
  //   EntryOrderController.updateEntryOrderImageLink
  // )
  // app.post('/getEntryOrders',
  //   EntryOrderController.getEntryOrders
  // )
  // app.post('/getAllEntryOrders',
  //   EntryOrderController.getAllEntryOrders
  // )
  // app.post('/deleteEntryOrder',
  //   EntryOrderController.deleteEntryOrder
  // )
  // // create investment plan
  // app.post('/createInvestmentPlan',
  //   InvestmentPlanController.createInvestmentPlan
  // )
  // app.post('/updatePendingInvestmentPlanImageLink',
  //   InvestmentPlanController.updatePendingInvestmentPlanImageLink
  // )
  // app.post('/getInvestmentPlans',
  //   InvestmentPlanController.getInvestmentPlans
  // )
  // app.post('/getConfirmedInvestmentPlans',
  //   InvestmentPlanController.getConfirmedInvestmentPlans
  // )
  // app.post('/investmentPlanExpired',
  //   InvestmentPlanController.investmentPlanExpired
  // )
}
