const admin = require('firebase-admin')
const FieldValue = require('firebase-admin').firestore.FieldValue
// get serviceAccount json file  from config
var db = admin.firestore()

// const user = require('../models/User.js')
// const userData = user.data

module.exports = {
    async getAllUsers (req, res) {
        try {
            // async function listAllUsers(nextPageToken) {
            //     // List batch of users, 1000 at a time.
            //     var listUsersResult = await admin.auth().listUsers(1000, nextPageToken)
            //     var data = []
            //     listUsersResult.users.forEach((userRecord) => {
            //         console.log("user", userRecord.toJSON())
            //         data.push(userRecord.toJSON())
            //     })
            //     if (listUsersResult.pageToken) {
            //         // List next batch of users.
            //         listAllUsers(listUsersResult.pageToken)
            //     }
            // }
            // listAllUsers()
            var data = []
            var query = await db.collection('users').get()
            query.forEach(doc => {
                var r = doc.data()
                r.docId = doc.id
                data.push(r)
            })
            console.log(data)
            res.status(200).send(data)
        } catch(err) {
            console.log("Error listing users:", error)
            res.status(500).send(err)
        }
    },
    async updateUser (req, res) {
        console.log(req.body)
        try {
            var userRef = db.collection('users').doc(req.body.docId)
            await userRef.update({
                userUid: req.body.userUid,
                name: req.body.name,
                realName: req.body.realName,
                gender: req.body.gender,
                birthDate: req.body.birthDate,
                licenseNumber: req.body.licenseNumber,
                licenseType: req.body.licenseType,
                address: req.body.address,
                phoneNumber: req.body.phoneNumber,
                // phoneVerified: true,
                // informationFilled: true,
                userRole: req.body.userRole,
                countryName: req.body.countryName,
                countryCode: req.body.countryCode
            })
            res.status(200).send('Updated !')
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    },
    async createUser (req, res) {
        try {
            // console.log(req.body)
            var userRecord = await admin.auth().createUser({
                email: req.body.email,
                emailVerified: true,
                password: req.body.password,
                displayName: req.body.displayName
            })
            console.log("Successfully created new user:", userRecord.uid)
            var docRef = await db.collection('users').add({
                userUid: userRecord.uid,
                name: req.body.displayName,
                realName: req.body.realName,
                gender: req.body.gender,
                birthDate: req.body.birthDate,
                licenseNumber: req.body.licenseNumber,
                licenseType: req.body.licenseType,
                address: req.body.address,
                phoneNumber: req.body.phoneNumber,
                phoneVerified: true,
                informationFilled: true,
                //
                userRole: req.body.userRole,
                countryName: req.body.countryName,
                countryCode: req.body.countryCode
            })
            var data = {
                status: 'success',
                message: 'User Added !',
                docId: docRef.id
            }
            res.status(200).send(data)
        } catch (error) {
            console.log("Error creating new user:", error)
            var data = {
                status: 'error',
                message: error
            }
            res.send(data)
        }
    }
}
