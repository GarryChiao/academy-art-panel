const admin = require('firebase-admin')
const moment = require('moment-business-days')
const FieldValue = require('firebase-admin').firestore.FieldValue
// get serviceAccount json file  from config
var db = admin.firestore()
const System = require('../models/System.js')
const config = require('../config/config')

module.exports = {
    dailyInterestReturn () {
        try {
            // console.log(moment(Date.now()).isBusinessDay())
            // if (true) {
            if (moment(Date.now()).isBusinessDay()) {
                var envSetitngsRef = db.collection('envSettings').doc('envSettings')
                var returnRate = ''
                var getDoc = envSetitngsRef.get()
                    .then(doc => {
                        // get env config
                        if (!doc.exists) {
                            returnRate = config.defaultReturnRate
                        } else {
                            var data = doc.data()
                            returnRate = data.returnRate
                        }
                        // console.log(returnRate)
                        // set date time format
                        var now = new Date()
                        var mm = now.getMonth() + 1 // getMonth() is zero-based
                        var dd = now.getDate()
                        var hour = now.getHours()
                        var min = now.getMinutes()
                        var sec = now.getSeconds()
                        var today = now.getFullYear() + '/' + (mm > 9 ? '' : '0') + mm + '/' + (dd > 9 ? '' : '0') + dd
                        var createDateTime = now.getFullYear() + '/' + (mm > 9 ? '' : '0') + mm + '/' + (dd > 9 ? '' : '0') + dd + ' ' + (hour > 9 ? '' : '0') + hour + ':' + (min > 9 ? '' : '0') + min + ':' + (sec > 9 ? '' : '0') + sec
                        // get confirmed investment plans, calculate each daily interest return
                        var confirmedInvestmentPlans = db.collection('investmentPlan')
                        var query = confirmedInvestmentPlans.where('status', '==', 'Confirmed').get()
                        .then(result => {
                            var count = 1
                            var size = result.size
                            result.forEach(doc => {
                                // console.log(doc.id, '=>', doc.data())
                                var data = doc.data()
                                // calculate daily interest return
                                if (data.returnRate) {
                                    var interestReturned = Math.round((data.addUpInvAmount - data.returnedAmount) * data.returnRate)    
                                } else{
                                    var interestReturned = Math.round((data.addUpInvAmount - data.returnedAmount) * returnRate)
                                }
                                // create const pormise for interest return
                                const dailyInterestReturn = new Promise((resolve, reject) => {
                                    const interest = {
                                        today: today,
                                        createDateTime: createDateTime,
                                        interestReturned: interestReturned,
                                        docId: doc.id,
                                        docData: data
                                    }
                                    resolve(interest)
                                })
                                // create a function to create daily interest return
                                async function createDailyInterestReturn (data) {
                                    return result = await System.dailyInterestReturn({
                                        date: data.today,
                                        createDateTime: data.createDateTime,
                                        userUid: data.docData.userUid,
                                        serialNumber: data.docData.serialNumber,
                                        tsfDepositAmount: data.docData.addUpTSFDeposit,
                                        planTotalAmount: data.docData.addUpInvAmount,
                                        interestReturned: data.interestReturned
                                    })
                                }
                                // create a function to update investment plan return amount
                                async function updateInvestmentReturnAmount (data) {
                                    try {
                                        var investmentPlanRef = db.collection('investmentPlan').doc(data.docId)
                                        var investmentPlan = await db.runTransaction(t => {
                                            return t.get(investmentPlanRef)
                                                .then(doc => {
                                                    // Update investment plan's returned amount
                                                    var newReturnAmount = doc.data().returnedAmount + data.interestReturned
                                                    t.update(investmentPlanRef, { returnedAmount: newReturnAmount })
                                                })
                                            })
                                        return Promise.resolve('Successfully updated investment plan return amount.')
                                    } catch (error) {
                                        return Promise.reject('Error occurred while updating investment plan return amount : ' + error)
                                    }
                                }
                                // create a function to update user's total earnings
                                async function updateUserTotalEarnings (data) {
                                    try {
                                        var userPlanRef = db.collection('users').doc(data.docData.userUid)
                                        var userPlan = await db.runTransaction(t => {
                                            return t.get(userPlanRef)
                                                .then(doc => {
                                                    // Update user's total earnings
                                                    var newTotalEarningsNow = doc.data().totalEarningsNow + data.interestReturned
                                                    var newTotalEarned = doc.data().totalEarned + data.interestReturned
                                                    t.update(userPlanRef, {
                                                        totalEarningsNow: newTotalEarningsNow,
                                                        totalEarned: newTotalEarned
                                                    })
                                                })
                                        })
                                        return Promise.resolve('Successfully updated users total earning fields.')
                                    } catch (error) {
                                        return Promise.reject('Error Occurred while updaing users total earning fields : ' + error)
                                    }
                                }
                                // run the above functions
                                async function operationStart () {
                                    try {
                                        let data = await dailyInterestReturn
                                        var result = await Promise.all([
                                            createDailyInterestReturn(data),
                                            updateInvestmentReturnAmount(data),
                                            updateUserTotalEarnings(data)
                                        ])
                                        console.log(result)
                                    } catch (error) {
                                        console.log(error)
                                    }
                                }
                                // after all records have been settled, create each user's daily total earnings
                                async function createUserDailyEarnings () {
                                    try {
                                        var userRef = db.collection('users')
                                        var userResults = await userRef.get()
                                        userResults.forEach(doc => {
                                            var userUid = doc.id
                                            var dailyInterestRef = db.collection('dailyInterestReturn')
                                                .where('userUid', '==', userUid)
                                                .where('date', '==', today)
                                                .get()
                                                .then(dailyInterestResult => {
                                                    var totalEarnedToday = 0
                                                    dailyInterestResult.forEach(document => {
                                                        totalEarnedToday = totalEarnedToday + document.data().interestReturned
                                                    })
                                                    System.dailyUserEarnings({
                                                        date: today,
                                                        createDateTime: createDateTime,
                                                        userUid: userUid,
                                                        totalEarnedToday: totalEarnedToday
                                                    }).then(result => {
                                                        console.log(result)
                                                    }).catch(error => {
                                                        console.log(error)
                                                    })
                                                })
                                        })
                                        return Promise.resolve('Successfully updated users total earning fields.')
                                    } catch (error) {
                                        console.log('Error getting documents', err)
                                        return Promise.reject('Error Occurred while updaing users total earning fields : ' + error)
                                    }
                                }
                                (async () => {
                                    await operationStart()
                                    // after all records have been settled
                                    if (count == size) {
                                        await createUserDailyEarnings()
                                        console.log('end')
                                    } else {
                                        count++
                                    }
                                })()
                            })
                        })
                        .catch(err => {
                            console.log('Error getting documents in daily interest return', err)
                        })
                    })
                    .catch(err => {
                        console.log('Error getting document', err)
                    })
                } else {
                console.log('Not business Day ! No interest returned.')
            }
        } catch (err) {
            console.log(err)
        }
    },
    async calculateMonthlyUserReport () {
        try {
            // create a monthly report for each user
            // including total earned, total spent, total deposit
            var now = new Date()
            var mm = now.getMonth() // getMonth() is zero-based, we fetch the last month data, no need to add 1 here
            var mmPlus = now.getMonth() + 1
            var dateStart = now.getFullYear() + '/' + (mm > 9 ? '' : '0') + mm 
            var dateEnd = now.getFullYear() + '/' + (mmPlus > 9 ? '' : '0') + mmPlus 
            
            console.log(dateStart)
            console.log(dateEnd)
            var userRef = db.collection('users')
            var userResults = await userRef.get()
            var users = []
            var totalEarned, totalConsumption, totalDeposit
            userResults.forEach(doc => {
                var userUid = doc.id
                users.push(doc.id)
            })

            for (var i = 0; i < users.length; i++) {
                totalEarned = 0
                totalConsumption = 0
                totalDeposit = 0
                // calculating interest
                var queryInterest = await db.collection('dailyInterestReturn')
                    .where('date', '>=', dateStart)
                    .where('date', '<=', dateEnd)
                    .where('userUid', '==', users[i])
                    .get()
                if (queryInterest.size > 0) {
                    for (j = 0; j < queryInterest.size; j++) {
                        totalEarned += parseInt(queryInterest.docs[j].data().interestReturned)
                    }
                }
                // calculating user total spent 
                var queryEntryOrder = await db.collection('entryOrder')
                    .where('invDate', '>=', dateStart)
                    .where('invDate', '<=', dateEnd)
                    .where('userUid', '==', users[i])
                    .get()
                if (queryEntryOrder.size > 0) {
                    for (j = 0; j < queryEntryOrder.size; j++) {
                        totalConsumption += parseInt(queryEntryOrder.docs[j].data().invAmount)
                    }
                }
                // calculating user total deposit to TSF
                var queryInvestmentPlan = await db.collection('investmentPlan')
                    .where('createDateTime', '>=', dateStart)
                    .where('createDateTime', '<=', dateEnd)
                    .where('status', '==', 'Confirmed')
                    .where('userUid', '==', users[i])
                    .get()
                if (queryInvestmentPlan.size > 0) {
                    for (j = 0; j < queryInvestmentPlan.size; j++) {
                        totalDeposit += parseInt(queryInvestmentPlan.docs[j].data().addUpTSFDeposit)
                    } 
                }
                // creating monthly user report
                var data = {
                    userUid: users[i],
                    dateTime: dateStart,
                    totalEarned: totalEarned,
                    totalConsumption: totalConsumption,
                    totalDeposit: totalDeposit
                }
                await db.collection('monthlyUserReport').add(data)
                // console.log(users[i], totalEarned, totalConsumption, totalDeposit)
            }
            // console.log(users.length)
        } catch(error) {
            console.log(error)
        }
    },
    async adminFunction () {
        // var query = await db.collection('dailyUserEarnings').where('userUid', '==', 'M7OpGfIuboaNZHbF2WbTyK6CSGW2').get()
        // query.forEach(doc => {
        //     console.log(doc.id)
        //     var deleteDoc = db.collection('dailyUserEarnings').doc(doc.id).delete()
        // })
    }
}
