const config = require('../config/config')
const admin = require('firebase-admin')
const FieldValue = require('firebase-admin').firestore.FieldValue
// get serviceAccount json file from config
var db = admin.firestore()

// const user = require('../models/User.js')
// const userData = user.data

module.exports = {
    async index (req, res) {
        function listAllUsers (nextPageToken) {
        // List batch of users, 1000 at a time.
        admin.auth().listUsers(1000, nextPageToken)
            .then(function (listUsersResult) {
            listUsersResult.users.forEach(function(userRecord) {
                console.log("user", userRecord.toJSON())
            })
            if (listUsersResult.pageToken) {
                // List next batch of users.
                listAllUsers(listUsersResult.pageToken)
            }
            })
            .catch(function (error) {
                console.log("Error listing users:", error)
            })
        }
        // Start listing users from the beginning, 1000 at a time.
        listAllUsers()
    },
    getAllPendingInvestmentPlans (req, res) {
        var investmentPlanRef = db.collection('investmentPlan')
        var query = investmentPlanRef
            .where('status', '==' ,'Pending')
            .get()
            .then(response => { 
            var data = []
            response.forEach(doc => {
                data.push(doc.data())
                // console.log(doc.id, '=>', doc.data())
            })
            // console.log(data)
            res.send(data)
            })
            .catch(err => {
                console.log('Error getting entry order documents', err)
            })
    },
    confirmPendingInvestmentPlan (req, res) {
        // on confirmation, update documents status to confirmed
        var investmentPlanRef = db.collection("investmentPlan").doc(req.body.userUid + req.body.serialNumber)
        
        return investmentPlanRef.update({
            status: 'Confirmed',
            confirmationDateTime: Date.now()
        }).then(function() {
            res.status(200).send('Investment Plan Confirmed')
        }).catch(function(error) {
            // The document probably doesn't exist.
            res.status(500).send('Investment Plan Confirmation error')
            console.error("Investment Plan Confirmation error : ", error)
        })
    },
    declinePendingInvestmentPlan (req, res) {
        // on confirmation, update documents status to confirmed
        var investmentPlanRef = db.collection("investmentPlan").doc(req.body.userUid + req.body.serialNumber)
        
        return investmentPlanRef.update({
            status: 'Declined',
            declinedDateTime: Date.now()
        }).then(function() {
            res.status(200).send('Investment Plan Declined')
        }).catch(function(error) {
            // The document probably doesn't exist.
            res.status(500).send('Investment Plan Confirmation error')
            console.error("Investment Plan Confirmation error : ", error)
        })
    },
    getEnvSettings (req, res) {
        // console.log(config.defaultReturnRate)
        var envSetitngsRef = db.collection('envSettings').doc('envSettings')
        var getDoc = envSetitngsRef.get()
            .then(doc => {
                if (!doc.exists) {
                    var data = JSON.stringify({ 
                        returnRate: config.defaultReturnRate
                    })
                    res.send(data)
                    console.log('No such document!')
                } else {
                    var data = doc.data()
                    res.send(data)
                }
            })
            .catch(err => {
                console.log('Error getting document', err)
            })
    },
    updateEnvSettings (req, res) {
        var envSetitngsRef = db.collection('envSettings').doc('envSettings')
        return envSetitngsRef.set({
            returnRate: req.body.returnRate
        }).then(function() {
            res.status(200).send('Env Settings Updated')
        }).catch(function(error) {
            // The document probably doesn't exist.
            res.status(500).send('Errors occured while updating Env Settings')
            console.error("Errors occured while updating Env Settings : ", error)
        })
    },
    getAllPendingWithdrawTotalEarnings (req, res) {
        var withdrawTotalEarningsRef = db.collection('withdrawRecord')
        var query = withdrawTotalEarningsRef
            .where('status', '==' ,'Pending')
            .get()
            .then(response => { 
            var data = []
            response.forEach(doc => {
                data.push(doc.data())
                // console.log(doc.id, '=>', doc.data())
            })
            // console.log(data)
            res.send(data)
            })
            .catch(err => {
                console.log('Error getting entry order documents', err)
            })
    },
    confirmPendingWithdrawTotalEarnings (req, res) {
        var withdrawTotalEarningsRef = db.collection('withdrawRecord').doc(req.body.userUid + req.body.requestDateTime)
        var query = withdrawTotalEarningsRef
            .update({
                status: 'Confirmed'
            })
            .then(response => {
                var userRef = db.collection('users').doc(req.body.userUid)
                var transaction = db.runTransaction(t => {
                    return t.get(userRef)
                        .then(doc => {
                            // Add one person to the city population
                            var newTotalWithdrawn = doc.data().totalWithdrawn + doc.data().totalEarningsNow
                            t.update(userRef, {
                                // push all total earnings to total withdrawn
                                totalEarningsNow: 0,
                                totalWithdrawn: newTotalWithdrawn
                            })
                        })
                  }).then(result => {
                    console.log('Successfully turned total earnings to total withdrawn.')
                  }).catch(err => {
                    console.log('Error occurred while turning total earnings to total withdrawn:', err)
                  })
                res.status(200).send('Withdraw Request Confirmed.')
            })
            .catch(err => {
                console.log('Error getting entry order documents', err)
            })
    },
    declinePendingWithdrawTotalEarnings (req, res) {
        var withdrawTotalEarningsRef = db.collection('withdrawRecord').doc(req.body.userUid + req.body.requestDateTime)
        var query = withdrawTotalEarningsRef
            .update({
                status: 'Declined'
            })
            .then(response => {
                res.status(200).send('Withdraw Request Declined.')
                console.log(response)
            })
            .catch(err => {
                console.log('Error getting entry order documents', err)
            })
    },
    // get user's investment plans
    async fetchSelectedUserInvestmentPlans (req, res) {
        try {
            var response = []
            var query = await db.collection('investmentPlan')
                .where('userUid', '==', req.body.userUid)
                .where('fetched', '==', true)
                .get()
            query.forEach(doc => {
                response.push(doc.data())
                // console.log(doc.data())
            })
            res.status(200).send(response)
        } catch(error) {
            res.status(500).send(error)
            console.log(error)
        }
    },
    async fetchSelectedUserEntryOrders (req, res) {
        try {
            var response = []
            var query = await db.collection('entryOrder')
                .where('userUid', '==', req.body.userUid)
                .where('isPlanned', '==', false)
                .get()
            query.forEach(doc => {
                response.push(doc.data())
                // console.log(doc.data())
            })
            res.status(200).send(response)
        } catch(error) {
            res.status(500).send(error)
            console.log(error)
        }
    },
    async updateInvestmentPlanReturnRate (req, res) {
        // console.log(req.body)
        var invPlanRef = db.collection('investmentPlan').doc(req.body.userUid + req.body.serialNumber)
        return invPlanRef.update({
            returnRate: req.body.returnRate
        }).then(function() {
            res.status(200).send('return rate Updated')
        }).catch(function(error) {
            // The document probably doesn't exist.
            res.status(500).send('Errors occured while updating Env Settings')
            console.error("Errors occured while updating Env Settings : ", error)
        })
    },
    async verifyInvestmentPlanWithoutRemittance (req, res) {
        // console.log(req.body)
        var invPlanRef = db.collection('investmentPlan').doc(req.body.userUid + req.body.serialNumber)
        return invPlanRef.update({
            remittanceProofImageUrl: 'verified by admin',
            status: 'Confirmed'
        }).then(function() {
            res.status(200).send('Investment Plan Verified')
        }).catch(function(error) {
            // The document probably doesn't exist.
            res.status(500).send('Errors occured while updating Env Settings')
            console.error("Errors occured while updating Env Settings : ", error)
        })
    }
}
