const admin = require('firebase-admin')
const moment = require('moment')
const FieldValue = require('firebase-admin').firestore.FieldValue
var db = admin.firestore()

module.exports = {
    async getMemberCount (req, res) {
        try {
            var query = await db.collection('users').get()
            var response = {
                count: query.size
            }
            res.status(200).send(response)
        } catch(error) {
            console.log(error)
            res.status(500).send(error)
        }
    },
    async getTotalTSFDeposit (req, res) {
        try {
            var query = await db.collection('investmentPlan').where('status' ,'==', 'Confirmed').get()
            var total = 0
            query.forEach(doc => {
                console.log(doc.data().addUpTSFDeposit)
                total += doc.data().addUpTSFDeposit
            })
            var response = {
                totalTSFDeposit: total
            }
            res.status(200).send(response)
        } catch(error) {
            console.log(error)
            res.status(500).send(error)
        }
    }
}
