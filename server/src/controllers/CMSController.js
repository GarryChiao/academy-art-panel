const admin = require('firebase-admin')
const FieldValue = require('firebase-admin').firestore.FieldValue
// get serviceAccount json file  from config
var db = admin.firestore()

// const user = require('../models/User.js')
// const userData = user.data

module.exports = {
    async getContent (req, res) {
        try {
            var contentRef = db.collection('webContent').doc('webContent');
            var doc = await contentRef.get()
            // console.log(doc.data())
            res.status(200).send(doc.data())
        } catch (err) {
            res.status(500).send(err)
            console.log(err)
        }
    },
    async updateContent (req, res) {
        try {
            var data = {
                // slider 1
                slider_1_title_1: req.body.slider_1_title_1,
                slider_1_title_2: req.body.slider_1_title_2,
                slider_1_text: req.body.slider_1_text,
                // slider 2
                slider_2_title_1: req.body.slider_2_title_1,
                slider_2_text: req.body.slider_2_text,
                // slider 3
                slider_3_title_1: req.body.slider_3_title_1,
                slider_3_title_2: req.body.slider_3_title_2,
                slider_3_text: req.body.slider_3_text,
                // status
                // status 1
                status_title_1: req.body.status_title_1,
                status_quantity_1: req.body.status_quantity_1,
                // status 2
                status_title_2: req.body.status_title_2,
                status_quantity_2: req.body.status_quantity_2,
                // status 3
                status_title_3: req.body.status_title_3,
                status_quantity_3: req.body.status_quantity_3,
                // status 4
                status_title_4: req.body.status_title_4,
                status_quantity_4: req.body.status_quantity_4,
                // user experience
                // user 1
                user_1_name: req.body.user_1_name,
                user_1_sub_title: req.body.user_1_sub_title,
                user_1_experience: req.body.user_1_experience,
                // user 2
                user_2_name: req.body.user_2_name,
                user_2_sub_title: req.body.user_2_sub_title,
                user_2_experience: req.body.user_2_experience,
                // user 3
                user_3_name: req.body.user_3_name,
                user_3_sub_title: req.body.user_3_sub_title,
                user_3_experience: req.body.user_3_experience,
                // why us
                why_us_title: req.body.why_us_title,
                why_us_text: req.body.why_us_text,
                why_us_sub_title_1: req.body.why_us_sub_title_1,
                why_us_sub_text_1: req.body.why_us_sub_text_1,
                why_us_sub_title_2: req.body.why_us_sub_title_2,
                why_us_sub_text_2: req.body.why_us_sub_text_2,
                why_us_sub_title_3: req.body.why_us_sub_title_3,
                why_us_sub_text_3: req.body.why_us_sub_text_3
            }
            // Get the `FieldValue` object
            var FieldValue = require('firebase-admin').firestore.FieldValue;
            // Create a document reference
            var contentRef = db.collection('webContent').doc('webContent');
            // Update the timestamp field with the value from the server
            await contentRef.update(data)
            res.status(200).send('Update content successful')
        } catch(error) {
            res.status(500).send(error)
            console.log(error)
        }
    },
    async updateContentImage (req, res) {
        console.log(req.body)
        try {
            if (req.body.userOneImage) {
                console.log('one')
                await db.collection('webContent').doc('webContent').update({
                    userOneImage: req.body.userOneImage
                })
            }
            if (req.body.userTwoImage) {
                console.log('two')
                await db.collection('webContent').doc('webContent').update({
                    userTwoImage: req.body.userTwoImage
                })
            }
            if (req.body.userThreeImage) {
                console.log('three')
                await db.collection('webContent').doc('webContent').update({
                    userThreeImage: req.body.userThreeImage
                })
            }
            if (req.body.whyUsImage) {
                console.log('four')
                await db.collection('webContent').doc('webContent').update({
                    whyUsImage: req.body.whyUsImage
                })
            }
            res.status(200).send('Update content successful')
        } catch(error) {
            res.status(500).send(error)
            console.log(error)
        }
    },
    async getClientImage (req, res) {
        try {
            var response = []
            var query = await db.collection('webContentClients').get();
            query.forEach(doc => {
                var data = doc.data()
                data.id = doc.id
                response.push(data)
            })
            // console.log(response)
            res.status(200).send(response)
        } catch(error) {
            console.log(error)
            res.status(500).send(error)
        }
    },
    async addClientImage (req, res) {
        try {
            var clientsRef = db.collection('webContentClients').doc();
            await clientsRef.set({
                link: req.body.link,
                imageLink: req.body.imageLink
            });
            res.status(200).send('Successfully Added')
        } catch(error) {
            console.log(error)
            res.status(500).send(error)
        }
    },
    async deleteClientImage (req, res) {
        try {
            await db.collection('webContentClients').doc(req.body.id).delete()
            res.status(200).send('Successfully Deleted')
        } catch(error) {
            console.log(error)
            res.status(500).send(error)
        }
    },
}
