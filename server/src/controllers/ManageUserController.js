const User = require('../models/User.js')
const admin = require('firebase-admin')
const moment = require('moment')
const FieldValue = require('firebase-admin').firestore.FieldValue
var db = admin.firestore()

module.exports = {
  async getAllUsers (req, res) {
    try {
      async function listAllUsers(nextPageToken) {
        // List batch of users, 1000 at a time.
        var listUsersResult = await admin.auth().listUsers(1000, nextPageToken)
        if (listUsersResult.pageToken) {
          // List next batch of users.
          listAllUsers(listUsersResult.pageToken)
        }
        var userInfos = await db.collection('users').get()

        for (var index = 0; index < listUsersResult.users.length; index++) {
          for (var h = 0; h < userInfos.docs.length; h++) {
            // console.log(userInfos.docs[h].id)
            if (userInfos.docs[h].id === listUsersResult.users[index].uid ) {
              var phoneNumber = {
                phoneNumber: userInfos.docs[h].data().phoneNumber
              }
              listUsersResult.users[index] = Object.assign(
                userInfos.docs[h].data(),
                listUsersResult.users[index],
                phoneNumber
              )
            }
          }
        }
        // console.log(listUsersResult.users)
        return listUsersResult.users
      }
      // Start listing users from the beginning, 1000 at a time.
      var response = await listAllUsers()
      // console.log(response)
      res.status(200).send(response)
    } catch (err) {
      console.log("Error listing users:", err)
      res.status(500).send(Promise.reject("Error listing users:" + error))
    }
  },
  async adminUpdateUser (req, res) {
    try {
      // console.log(req.body)
      var data = {
        realName: req.body.realName,
        gender: req.body.gender,
        birthDate: req.body.birthDate,
        licenseNumber: req.body.licenseNumber,
        licenseType: req.body.licenseType,
        address: req.body.address,
        phoneNumber: req.body.phoneNumber,
        // 20180625 added introducer code and shareholder
        introducerCode: req.body.introducerCode,
        shareholder: req.body.shareholder,
        bankCode: req.body.bankCode,
        bankName: req.body.bankName,
        bankAccount: req.body.bankAccount,
        authority: req.body.authority, // user authority wont change
        timestamp: FieldValue.serverTimestamp()
      }
      
      await db.collection('users').doc(req.body.uid).update(data)
      res.status(200).send('User info updated!')
    } catch (err) {
      console.log(err)
      res.status(500).send('error while updating :' + err)
    }
  },
  async adminAddNewUser (req, res) {
    try {
      // firebase authentication add user
      function convertPhone (phoneNum) {
        while (phoneNum.charAt(0) === '0') {
          phoneNum = phoneNum.substr(1)
        }
        var response = "+886" + phoneNum
        return response
      }
      // var phoneNum = phone(req.body.phoneNumber ,'USA')
      console.log(convertPhone(req.body.phoneNumber))
      // console.log(phoneNum)
      var userRecord = await admin.auth().createUser({
        email: req.body.email,
        emailVerified: false,
        phoneNumber: convertPhone(req.body.phoneNumber),
        password: req.body.password,
        displayName: req.body.displayName,
        photoURL: "http://www.example.com/12345678/photo.png",
        disabled: false
      })
      console.log('continue adding info')
      // add other user infos
      var data = {
        userUid: userRecord.uid,
        realName: req.body.realName,
        gender: req.body.gender,
        birthDate: req.body.birthDate,
        licenseNumber: req.body.licenseNumber,
        licenseType: req.body.licenseType,
        // licenseImageDownloadURL: '/static/img/license.png',
        licenseVerified: false,
        address: req.body.address,
        phoneNumber: req.body.phoneNumber,
        bankCode: req.body.bankCode,
        bankName: req.body.bankName,
        bankAccount: req.body.bankAccount,
        authority: req.body.authority,
        // on user withdrawing their earning
        // 20180625 added introducer
        introducerCode: req.body.introducerCode,
        shareholder: req.body.shareholder,
        totalWithdrawn: 0,
        // on interest returned from tsf
        totalEarningsNow: 0,
        totalEarned: 0,
        //
        timestamp: FieldValue.serverTimestamp()
      }
      var setUser = await db.collection('users').doc(userRecord.uid).set(data)
      console.log(setUser)
      var response = {
        status: 'success',
        message: 'Successfully Added User',
        userUid: userRecord.uid
      }
      res.send(response)
    } catch (err) {
      console.log(err)
      var response = {
        status: 'error',
        errorMessage: err
      }
      res.send(response)
    }
  },
  async adminDeleteUser (req, res) {
    admin.auth().deleteUser(req.body.userUid)
      .then(() => {
        var deleteDoc = db.collection('users').doc(req.body.userUid).delete()
        res.status(200).send('Successfully deleted user')
        console.log("Successfully deleted user");
      })
      .catch((error) => {
        res.status(500).send("Error deleting user:" + error)
        console.log("Error deleting user:", error);
      });
  },
  async getLicensePendingUsers (req, res) {
    try {
      var licensePendingUserRef = db.collection('users')
      var response = []
      var query = await licensePendingUserRef.where('licenseVerified', '==', false).get()
      console.log(query.size)
      for (var index = 0; index < query.size; index++) {
        console.log(query.docs[index].data().userUid)
        var userRecord = await admin.auth().getUser(query.docs[index].data().userUid)
        var data = Object.assign(query.docs[index].data(), userRecord.toJSON())
        response.push(data)
      }
      res.status(200).send(response)
    } catch (err) {
      console.log("Error listing users:", err)
      res.status(500).send(Promise.reject("Error listing users:" + error))
    }
  },
  async confirmLicensePendingUser (req, res) {
    try {
      await db.collection('users').doc(req.body.userUid).update({
        licenseVerified: true
      })
      var response = {
        status: 'success',
        message: 'User License Verified!'
      }
      res.send(response)
    } catch (error) {
      console.log(error)
      var response = {
        status: 'error',
        message: 'User License Verifying error : ' + error
      }
      res.send(response)
    }
  }
}
