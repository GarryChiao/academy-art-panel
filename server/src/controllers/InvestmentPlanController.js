const InvestmentPlan = require('../models/InvestmentPlan.js')
// const userData = user.data
const admin = require('firebase-admin')
var db = admin.firestore()

module.exports = {
    async createInvestmentPlan (req, res) {
        try {
            // console.log(req.body)
            await InvestmentPlan.create(req.body)
            // set planned entry order isPlanned true
            var entryOrderRef = ''
            var updateSingle = ''
            var entryOrders = req.body.entryOrders
            await entryOrders.forEach(id => {
                // set selected entry order isPlanned field to true
                // used userUid + entryOrderId as doc name
                entryOrderRef = db.collection('entryOrder').doc(req.body.userUid + id)
                updateSingle = entryOrderRef.update({ isPlanned: true })
            })
            res.send('investment plan created')
        } catch (err) {
        console.log(err)
        }
    },
    updatePendingInvestmentPlanImageLink (req, res) {
        try {
            var InvestmentPlanRef = db.collection(req.body.collection).doc(req.body.userUid + req.body.serialNumber)
            return InvestmentPlanRef.update({
                remittanceProofImageUrl: req.body.remittanceProofImageUrl,
                status: 'Pending'
            })
            .then(function() {
                res.send('Remittance Proof Added, Waiting for Verification')  
                console.log("Remittance Proof successfully updated!")
            })
            .catch(function(error) {
                // The document probably doesn't exist.
                res.send(error)
                console.error("Remittance Proof updating error: ", error)
            })
        } catch (err) {
            console.log(err)
        }
    },
    getInvestmentPlans (req, res) {
        var investmentPlanRef = db.collection('investmentPlan')
        var query = investmentPlanRef
            .where('userUid', '==', req.body.userUid)
            .where('fetched', '==', true)
            .get()
            .then(response => { 
                var data = []
                response.forEach(doc => {
                    data.push(doc.data())
                    // console.log(doc.id, '=>', doc.data())
                })
                // console.log(data)
                res.status(200).send(data)
            })
            .catch(err => {
                res.status(500).send(err)
                console.log('Error getting entry order documents', err)
            })
    },
    getConfirmedInvestmentPlans (req, res) {
        var investmentPlanRef = db.collection('investmentPlan')
        var query = investmentPlanRef
            .where('userUid', '==', req.body.userUid)
            .where('status', '==', 'Confirmed')
            .get()
            .then(response => { 
                var data = []
                response.forEach(doc => {
                    data.push(doc.data())
                    // console.log(doc.id, '=>', doc.data())
                })
                // console.log(data)
                res.status(200).send(data)
            })
            .catch(err => {
                res.status(500).send(err)
                console.log('Error getting entry order documents', err)
            })
    },
    async investmentPlanExpired (req, res) {
        console.log(req.body)
        var investmentPlanRef = db.collection('investmentPlan').doc(req.body.userUid + req.body.serialNumber)
        try {
            var query = await investmentPlanRef.get()
            var data = query.data()
            console.log(data)
            var entryOrders = data.entryOrders
            // console.log(entryOrders.length)
            for (var i = 0; i < entryOrders.length; i++ ) {
                console.log(i)
                var entryOrderRef = db.collection('entryOrder').doc(req.body.userUid + entryOrders[i])
                await entryOrderRef.update({
                    isPlanned: false 
                })
            }
            await investmentPlanRef.update({
                status: 'Expired',
                fetched: false
            })
            res.status(200).send('successfully set investment plan status to expired')
            
        } catch(error) {
            console.log(error)
        }
    }
}
