const User = require('../models/User.js')
const WithdrawRecord = require('../models/WithdrawRecord.js')
// const userData = user.data
const admin = require('firebase-admin')
const moment = require('moment')
var db = admin.firestore()

module.exports = {
  async indexUserInfo (req, res) {
    try {
      // create users using Firebase Admin SDK https://firebase.google.com/docs/auth/admin/manage-users
      // user model might be useless on firebase since everything's done by firebase
      var userRecord = await admin.auth().getUser(req.body.uid)
      var userInfo = await db.collection('users').doc(req.body.uid).get()
      // console.log(userInfo.data())
      if (userInfo.data()) {
        var data = Object.assign(userRecord.toJSON(), userInfo.data())
        // if the introducer code is assigned
        if (userInfo.data().introducerCode) {
          var introducerInfo = await db.collection('users').where('userCode', '==', userInfo.data().introducerCode).get()
          // console.log(introducerInfo)
          // if has reocrd
          if (introducerInfo.size !== 0) {
            // gets the introducer's displayName
            data.introducerName = introducerInfo.docs[0].data().realName
          }
        }
        // console.log(data)
        res.status(200).send(data)
      } else {
        var data = userRecord.toJSON()
        res.status(200).send(data)
      }
    } catch (error) {
      console.log("Error fetching user data:", error)
      res.status(500).send("Error fetching user data:" + error)
    }
  },
  async updateUserInfo (req, res) {
    try {
      // create users using Firebase Admin SDK https://firebase.google.com/docs/auth/admin/manage-users
      // user model might be useless on firebase since everything's done by firebase
      // console.log(req.body)
      if (req.body.formType === 'create') {
        try {
          var result = await User.create(req.body)
          res.status(200).send(result)  
        } catch (error) {
          res.status(500).send(error)  
        }
      } else if (req.body.formType === 'update') {
        try {
          var result = await User.update(req.body)
          res.status(200).send(result)  
        } catch (error) {
          console.log(error)
          res.status(500).send(error)  
        }
      }
    } catch (err) {
      console.log(err)
    }
  },
  async updateUserInfoLicenseImage (req, res) {
    try {
      // create users using Firebase Admin SDK https://firebase.google.com/docs/auth/admin/manage-users
      // user model might be useless on firebase since everything's done by firebase
      var userRef = db.collection('users').doc(req.body.uid)
      console.log(req.body)
      if (req.body.licenseImageDownloadURL_1) {
        userRef.update({
          licenseImageDownloadURL_1: req.body.licenseImageDownloadURL_1,
          // licenseVerified: false
        })
        .then(function() {
          console.log("License Image 1 successfully updated!")
        })
        .catch(function(error) {
          // The document probably doesn't exist.
          console.log("Image 1 updating error: ", error)
        })
      }
      if (req.body.licenseImageDownloadURL_2) {
        userRef.update({
          licenseImageDownloadURL_2: req.body.licenseImageDownloadURL_2,
          // licenseVerified: false
        })
        .then(function() {
          console.log("License Image 2 successfully updated!")
        })
        .catch(function(error) {
          // The document probably doesn't exist.
          console.log("Image 2 updating error: ", error)
        })
      }
      if (req.body.licenseImageDownloadURL_3) {
        userRef.update({
          licenseImageDownloadURL_3: req.body.licenseImageDownloadURL_3,
          // licenseVerified: false
        })
        .then(function() {
          console.log("License Image 3 successfully updated!")
        })
        .catch(function(error) {
          // The document probably doesn't exist.
          console.log("Image 3 updating error: ", error)
        })
      }
      if (req.body.licenseImageDownloadURL_4) {
        userRef.update({
          licenseImageDownloadURL_4: req.body.licenseImageDownloadURL_4,
          // licenseVerified: false
        })
        .then(function() {
          console.log("License Image 4 successfully updated!")
        })
        .catch(function(error) {
          // The document probably doesn't exist.
          console.log("Image 4 updating error: ", error)
        })
      }
      return userRef.update({
        licenseVerified: false,
      })
      .then(function() {
        res.status(200).send('updated, set license verified to false')
      })
      .catch(function(error) {
        // The document probably doesn't exist.
        res.status(500).send('set license verified to false got error')
        console.log("Image 4 updating error: ", error)
      })
    } catch (err) {
      console.log(err)
    }
  },
  withdrawTotalEarnings (req, res) {
    var userRef = db.collection('users').doc(req.body.uid)
    userRef.get()
      .then(result => {
        var data = result.data()
        // console.log(req.body)
        // console.log(data)
        WithdrawRecord.create({
          userUid: req.body.uid,
          userName: req.body.userName,
          bankCode: req.body.bankCode,
          bankAccount: req.body.bankAccount,
          bankName: req.body.bankName,
          withdrawAmount: data.totalEarningsNow, // total earnings from userRef
          requestDateTime: Date.now()
        })
        res.status(200).send('Waiting for verification.')
      })
      .catch(error => {
        console.log(error)
      })
  },
  getWithdrawTotalEarnings (req, res) {
    db.collection('withdrawRecord')
      .where('userUid', '==', req.body.uid)
      .where('status', '==', 'Pending')
      .get()
      .then(response => {
        var data = []
        response.forEach(doc => {
            data.push(doc.data())
            // console.log(doc.id, '=>', doc.data())
        })
        res.status(200).send(data)
        console.log(data)
      }).catch(error => {
        res.status(500).send(error)
        console.log(error)
      })
  },
  getDailyUserEarnings (req, res) {
    db.collection('dailyUserEarnings')
      .where('userUid', '==', req.body.uid)
      .limit(12)
      .get()
      .then(response => {
        var data = []
        var size = response.size
        if (size < 12) {
          for (var i = 12; i > size; i--) {
            data.push(0)
          }
        }
        response.forEach(doc => {
            data.push(doc.data().totalEarnedToday)
            // console.log(doc.id, '=>', doc.data())
        })
        res.status(200).send(data)
        console.log(data)
      }).catch(error => {
        res.status(500).send(error)
        console.log(error)
      })
  },
  async updateUserPhotoSticker (req, res) {
    var userRef = db.collection('users').doc(req.body.userUid);
    var updateSingle = userRef.update({ 
      userPhotoSticker: req.body.userPhotoSticker 
    });
  },
  async getMonthlyUserData (req, res) {
    try {
      var totalConsumption = []
      var totalDeposit = []
      var totalEarned = []
      var query = await db.collection('monthlyUserReport')
        .where('userUid' ,'==' ,req.body.userUid)
        .where('dateTime', '>=', req.body.queryDate)
        .get()
      if (query.size < 6) {
        for (var j = query.size; j < 6; j++) {
          totalConsumption.push(0)
          totalDeposit.push(0)
          totalEarned.push(0)
        }  
      }
      for (var i = 0; i < query.size; i++) {
        totalConsumption.push(query.docs[i].data().totalConsumption)
        totalDeposit.push(query.docs[i].data().totalDeposit)
        totalEarned.push(query.docs[i].data().totalEarned)
      }
      var data = {
        totalConsumption: totalConsumption,
        totalDeposit: totalDeposit,
        totalEarned: totalEarned
      }
      res.status(200).send(data)
    } catch(error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
}
