const EntryOrder = require('../models/EntryOrder.js')
// const userData = user.data
const admin = require('firebase-admin')
var db = admin.firestore()

module.exports = {
  async createEntryOrder (req, res) {
    try {
        var create = await EntryOrder.create(req.body)
        console.log(create)
        res.status(200).send('entry order created')
    } catch (err) {
      res.status(500).send(err)
      console.log(err)
    }
  },
  updateEntryOrderImageLink (req, res) {
    try {
        // use userUid + orderid as doc name
        var entryOrderRef = db.collection(req.body.collection).doc(req.body.userUid + req.body.entryOrderId)
        // console.log(req.body.entryOrderId)
        // console.log(req.body.userUid)
        return entryOrderRef.update({
            invoiceImageUrl: req.body.invoiceImage
        })
        .then(function() {
          res.send('Entry Order Added')  
          console.log("invoice Image successfully updated!")
        })
        .catch(function(error) {
          // The document probably doesn't exist.
          console.error("invoice Image updating error: ", error)
        })
      } catch (err) {
        console.log(err)
      }
  },
  getEntryOrders (req, res) {
    var entryOrderRef = db.collection('entryOrder')
    var query = entryOrderRef
        .where('isPlanned', '==', false)
        .where('userUid', '==', req.body.userUid)
        .get()
        .then(response => {
          var data = []
          response.forEach(doc => {
            data.push(doc.data())
            // console.log(doc.id, '=>', doc.data())
          })
          // console.log(data)
          res.send(data)
        })
        .catch(err => {
          console.log('Error getting entry order documents', err)
        })
  },
  getAllEntryOrders (req, res) {
    var entryOrderRef = db.collection('entryOrder')
    var query = entryOrderRef
        .where('userUid', '==', req.body.userUid)
        .get()
        .then(response => {
          var data = []
          response.forEach(doc => {
            data.push(doc.data())
            // console.log(doc.id, '=>', doc.data())
          })
          // console.log(data)
          res.send(data)
        })
        .catch(err => {
          console.log('Error getting entry order documents', err)
        })
  },
  async deleteEntryOrder (req, res) {
    try {
      var docName = req.body.userUid + req.body.entryOrderId
      console.log(docName)
      var query = await db.collection("entryOrder")
        .doc(docName)
        .delete()
        res.send('Delete Successful')
    } catch(error) {
      console.log(error)
    }
  }
}
