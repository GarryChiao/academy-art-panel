const admin = require('firebase-admin')
const moment = require('moment')
const FieldValue = require('firebase-admin').firestore.FieldValue
var db = admin.firestore()

module.exports = {
    async getContactForm (req, res) {
        try {
            var response = []
            var query = await db.collection('contactForm').get()
            for (var i = 0; i < query.size; i ++) {
                var data = query.docs[i].data()
                data.id = query.docs[i].id
                response.push(data)
            }
            res.status(200).send(response)
        } catch(error) {
            console.log(error)
            res.status(500).send(error)
        }
    },
    async deleteContactInfo(req, res) {
        try {
            await db.collection('contactForm').doc(req.body.id).delete()
            res.status(200).send('deleted')
        } catch(error) {
            console.log('error while deleting contact info: ' + error)
            res.status(500).send('error while deleting contact info: ' + error)
        }
    }
}
