// since the firebase has been initialized at
const config = require('../config/config')
const admin = require('firebase-admin')
const FieldValue = require('firebase-admin').firestore.FieldValue

var db = admin.firestore()

module.exports = { 
  async create (userData) {
    try {
      // check if the created user is the first, if yes, set authority to admin
      var hasUser = await db.collection('users').get()
      console.log(hasUser.size)
      if (hasUser.size === 0) {
        var userAuthority = 'SuperAdmin'
      } else {
        var userAuthority = 'NormalUser'
      }
      // 20180625 added introducer code, intented to create accelerator
      Number.prototype.pad = function(size) {
        var s = String(this);
        while (s.length < (size || 2)) {s = "0" + s;}
        return s;
      }
      var userCode = (hasUser.size).pad(6)
      // console.log(userCode)
      var data = {
        userUid: userData.uid,
        userCode: userCode,
        realName: userData.realName,
        gender: userData.gender,
        birthDate: userData.birthDate,
        licenseNumber: userData.licenseNumber,
        licenseType: userData.licenseType,
        address: userData.address,
        phoneNumber: userData.phoneNumber,
        bankCode: userData.bankCode,
        bankName: userData.bankName,
        bankAccount: userData.bankAccount,
        // 20180625 added field, not updatable
        introducerCode: userData.introducerCode,
        // 20180629 added shareholder
        shareholder: false,
        authority: userAuthority,
        // on user withdrawing their earning
        totalWithdrawn: 0,
        // on interest returned from tsf
        totalEarningsNow: 0,
        totalEarned: 0,
        //
        timestamp: FieldValue.serverTimestamp()
      }
      // console.log(data)
      await db.collection('users').doc(userData.uid).set(data)
      return Promise.resolve('User Info Created!')
    } catch (err) {
      console.log(err)
      return Promise.reject('Error occurred while creating User Info : ' + err)
      console.log(err)
    }
  },
  async update (userData) {
    try {
      // create users using Firebase Admin SDK https://firebase.google.com/docs/auth/admin/manage-users
      // but this part of information is not defined by firebase authentication
      // so the rest user info stored basically
      // var hasUser = await db.collection('users').get()
      
      var data = {
        userUid: userData.uid,
        realName: userData.realName,
        gender: userData.gender,
        birthDate: userData.birthDate,
        licenseNumber: userData.licenseNumber,
        licenseType: userData.licenseType,
        address: userData.address,
        phoneNumber: userData.phoneNumber,
        bankCode: userData.bankCode,
        bankName: userData.bankName,
        // 20180625 added introducer code field
        introducerCode: userData.introducerCode,
        bankName: userData.bankName,
        bankAccount: userData.bankAccount,
        // authority: userAuthority, // user authority wont change
        timestamp: FieldValue.serverTimestamp()
      }
      
      await db.collection('users').doc(userData.uid).update(data)
      return Promise.resolve('User info updated!')
    } catch (err) {
      return Promise.reject('Error occurred while updating user info : ' + err)
      console.log(err)
    }
  }
}
