// since the firebase has been initialized at
const config = require('../config/config')
const admin = require('firebase-admin')
const FieldValue = require('firebase-admin').firestore.FieldValue

var db = admin.firestore()

module.exports = {
  async dailyInterestReturn (details) {
    var data = {
      date: details.date,
      createDateTime: details.createDateTime,
      userUid: details.userUid,
      serialNumber: details.serialNumber,
      tsfDepositAmount: details.tsfDepositAmount,
      planTotalAmount: details.planTotalAmount,
      interestReturned: details.interestReturned
    }
    // console.log(data)
    // use userUid + serialNumber as doc name
    try {
      var ref = await db.collection('dailyInterestReturn').add(data)
      return Promise.resolve('Successfully created daily interest return.')
    } catch (error) {
      return Promise.reject('Error occurred while creating daily interest return : ' + error)
    }
  },
  async dailyUserEarnings (details) {
    // calculate yesterday's totalEarned, then add.
    var data = {
      date: details.date,
      createDateTime: details.createDateTime,
      userUid: details.userUid,
      totalEarnedToday: details.totalEarnedToday
    }
    // console.log(data)
    // use userUid + serialNumber as doc name
    try {
      var ref = await db.collection('dailyUserEarnings').add(data)
      return Promise.resolve('Successfully created daily user earnings.')
    } catch (error) {
      return Promise.reject('Error occurred while creating daily user earnings : ' + error)
    }
  }
}
