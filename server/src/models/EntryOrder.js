// since the firebase has been initialized at
const config = require('../config/config')
const admin = require('firebase-admin')
const FieldValue = require('firebase-admin').firestore.FieldValue

var db = admin.firestore()

module.exports = {
  async create (entryOrder) {
    try {
      // create users using Firebase Admin SDK https://firebase.google.com/docs/auth/admin/manage-users
      // but this part of information is not defined by firebase authentication
      // so the rest user info stored basically
      var data = {
        userUid: entryOrder.userUid,
        entryOrderId: entryOrder.entryOrderId,
        invDate: entryOrder.invDate, // invoice date
        sellerName: entryOrder.sellerName, // invoice seller name
        invAmount: entryOrder.invAmount, // the amount on a single invoice
        invQuantity: entryOrder.invQuantity,
        tsfDepositAmount: entryOrder.tsfDepositAmount,
        entryDateTime: entryOrder.entryDateTime,
        isPlanned: false
      }
      console.log('entryOrder.js create : ', data)
      await db.collection('entryOrder').doc(entryOrder.userUid + entryOrder.entryOrderId).set(data) // the first entryOrder points to the first collection, further add three layers to point to doc
      return Promise.resolve('success')
    } catch (err) {
      console.log(err)
      return Promise.reject(err)
    }
  }
}
