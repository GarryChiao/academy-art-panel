// since the firebase has been initialized at
const config = require('../config/config')
const admin = require('firebase-admin')
const FieldValue = require('firebase-admin').firestore.FieldValue
var db = admin.firestore()

module.exports = {
  create (withdrawRecord) {
    try {
      // create users using Firebase Admin SDK https://firebase.google.com/docs/auth/admin/manage-users
      // but this part of information is not defined by firebase authentication
      // save the user withdrawn records
      var data = {
        userUid: withdrawRecord.userUid,
        userName: withdrawRecord.userName,
        bankCode: withdrawRecord.bankCode,
        bankAccount: withdrawRecord.bankAccount,
        bankName: withdrawRecord.bankName,
        withdrawAmount: withdrawRecord.withdrawAmount,
        requestDateTime: withdrawRecord.requestDateTime,
        status: 'Pending',
        isVerified: false
      }
      // user userUid + requestDateTime as doc name
      db.collection('withdrawRecord').doc(withdrawRecord.userUid + withdrawRecord.requestDateTime).set(data)
        .then(response => {
            console.log('WithdrawRecord Inserted')
        }).catch(error => {
            console.log(error)
        })
    } catch (err) {
      console.log(err)
    }
  }
}
