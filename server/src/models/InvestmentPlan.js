// since the firebase has been initialized at
const config = require('../config/config')
const admin = require('firebase-admin')
const FieldValue = require('firebase-admin').firestore.FieldValue

var db = admin.firestore()

module.exports = {
  async create (investmentPlan) {
    try {
      // create users using Firebase Admin SDK https://firebase.google.com/docs/auth/admin/manage-users
      // but this part of information is not defined by firebase authentication
      // so the rest user info stored basically
      var data = {
        userUid: investmentPlan.userUid,
        serialNumber: investmentPlan.serialNumber, // use serial number as investment plan id
        displayName: investmentPlan.displayName,
        createDateTime: investmentPlan.createDateTime,
        addUpInvAmount: investmentPlan.addUpInvAmount,
        addUpTSFDeposit: investmentPlan.addUpTSFDeposit,
        verificationCode: investmentPlan.verificationCode,
        avgDailyInterest: investmentPlan.avgDailyInterest,
        highestMonthlyInterest: investmentPlan.highestMonthlyInterest,
        entryOrders: investmentPlan.entryOrders,
        status: 'CountingDown', // created for count downs
        fetched: true, // the first insert should be fetched, if it's expired, set to false
        returnedAmount: 0,
        returnRate: 0.0005
      }
      // console.log(data)
      // use userUid + serialNumber as doc name
      await db.collection('investmentPlan').doc(investmentPlan.userUid + investmentPlan.serialNumber).set(data) // the first entryOrder points to the first collection, further add three layers to point to doc
      return Promise.resolve('success')
    } catch (err) {
      console.log(err)
      return Promise.reject(err)
    }
  }
}
